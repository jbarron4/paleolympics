extends KinematicBody

# SIGNALS

signal coins_updated(new_coin_count)

# ATTRIBUTES
export(float) var walking_speed = 5 # m/s
export(float) var jump_impulse = 20
export(float) var gravity_scale = 1.0
export(float) var input_step = 0.5

# Horizontal movement limit, measured from the origin
export(float) var horizontal_limit = 6.0 # meters

onready var _default_gravity:Vector3 = ProjectSettings.get_setting("physics/3d/default_gravity_vector") * ProjectSettings.get_setting("physics/3d/default_gravity") * gravity_scale
onready var _animations = $AnimationTree.get("parameters/playback")
var _n_coins = 0
var _takes_damage = true

var _velocity := Vector3.ZERO
var _local_velocity_from_input := Vector3.ZERO
var _joystick_input := Vector2.ZERO
var _jump_just_pressed := false
var _attack_just_pressed := false



# GODOT METHODS
func _unhandled_input(_event):
	_joystick_input.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	#_joystick_input.y = Input.get_action_strength("ui_up") - Input.get_action_strength("ui_down")
	_jump_just_pressed = Input.is_action_just_pressed("feet")
	_attack_just_pressed = Input.is_action_just_pressed("hand")
	
	# Animations
	if _attack_just_pressed:
		var _current_node_name = _animations.get_current_node()
		$AnimationTree.set("parameters/" + _current_node_name + "/Attack/active", true)
		
		
	
func _physics_process(_delta):
	# Velocity from input
	var _target_local_velocity = Vector3( _joystick_input.x, 0 , 0).normalized() * walking_speed
	_local_velocity_from_input= _local_velocity_from_input.linear_interpolate( _target_local_velocity, input_step)
	
	
	var _global_velocity_from_input = transform.basis.xform( _local_velocity_from_input )
	_velocity.x = _global_velocity_from_input.x
	_velocity.z = _global_velocity_from_input.z
	
	# External forces
	if _jump_just_pressed and is_on_floor():
		_velocity.y = jump_impulse
	_velocity +=  _default_gravity
	
	_velocity = move_and_slide( _velocity, Vector3.UP )
	
	transform.origin.x = clamp(transform.origin.x, -horizontal_limit, horizontal_limit)
	


# CUSTOM METHODS
func _attack():
	for a in $AttackArea.get_overlapping_areas():
		if a.has_method("process_attack"):
			a.process_attack( self )

func add_coins( n_coins ):
	_n_coins += n_coins
	if(_n_coins<0):
		_n_coins = 0
	emit_signal("coins_updated", _n_coins)

func process_damage():
	$InvincibilityTimer.start()
	var _current_node_name = _animations.get_current_node()
	$AnimationTree.set("parameters/" + _current_node_name + "/Hurt/active", true)

func _on_InvincibilityTimer_timeout():
	_takes_damage = true
