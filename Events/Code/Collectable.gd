extends Area
class_name Collectable

# SIGNALS
signal collected( collector )

# ATTRIBUTES

# GODOT METHODS
func _on_body_entered(body):
	if body.has_method("collect"):
		if body.collect( self ):
			emit_signal("collected")
			queue_free()
		 

# CUSTOM METHODS



