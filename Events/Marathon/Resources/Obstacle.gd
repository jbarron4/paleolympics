extends Area

# SIGNALS
signal collided_with( source, collider )
signal attacked( source, attacker )

# ATTRIBUTES
export(bool) var destroy_on_collision = false
export(int) var coins_for_collision = -1
export(bool) var destroy_on_attack = false
export(int) var coins_for_attacking = 1
export(NodePath) var animation_player_node = "AnimationPlayer"
export(String) var collision_animation_name = "Collided"
export(String) var attack_animation_name = "Attacked"

var _animation_player:AnimationPlayer

# GODOT METHODS

func _on_body_entered(body):
	emit_signal("collided_with", body)
	
	if coins_for_collision != 0 and body.has_method("add_coins"):
			body.add_coins( coins_for_collision )
	
	if coins_for_collision<0 and body.has_method("process_damage"):
		body.process_damage()
	
	_play_animation(collision_animation_name, destroy_on_collision)

func _ready():
	if has_node(animation_player_node):
		_animation_player = get_node(animation_player_node)
		
	
# CUSTOM METHODS

func _on_selfdestruct_animation_finished( _animation ):
	queue_free()


func _play_animation(animation_name, destroy_at_finished = false):
	if is_instance_valid(_animation_player) and _animation_player.has_animation(animation_name):
		if destroy_at_finished:
			var error = _animation_player.connect("animation_finished", self, "_on_selfdestruct_animation_finished")
			if error != OK:
				print(name + "> Error connecting self-destruct call:" + error)
		_animation_player.play(animation_name)
	elif destroy_at_finished:
		_on_selfdestruct_animation_finished( "" )
	

func process_attack( attacker ):
	
	#DEBUG print(name + "> Attacked by " + attacker.name)
	if attacker.has_method("add_coins"):
		attacker.add_coins( coins_for_attacking )
	if destroy_on_attack:
		# Prevent colliding after being marked to destroy
		self.monitoring = false
		self.monitorable = false
	_play_animation( attack_animation_name, destroy_on_attack )
	emit_signal("attacked")
