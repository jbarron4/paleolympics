extends PathFollow

# SIGNALS
signal finished

# ATTRIBUTES
export(float) var speed = 20 # m/s

# GODOT METHODS
func _physics_process(delta):
	offset += speed * delta
	if unit_offset >= 1:
		emit_signal("finished")
		speed = 0
	
		

# CUSTOM METHODS
