extends CanvasLayer

# SIGNALS


# ATTRIBUTES

# GODOT METHODS
func _ready():
	_on_coins_updated(0)

# CUSTOM METHODS
func _on_coins_updated(new_coin_count):
	$Coins/CoinsCounter.text = str(new_coin_count)
