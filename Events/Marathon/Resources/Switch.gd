extends Spatial

# SIGNALS
signal activated

# ATTRIBUTES
# GODOT METHODS
func _ready():
	$AnimationPlayer.play("Activate",-1,0.0)

# CUSTOM METHODS
func _on_trigger():
	emit_signal("activated")
	if $AnimationPlayer.has_animation("Activate"):
		$AnimationPlayer.play("Activate")
